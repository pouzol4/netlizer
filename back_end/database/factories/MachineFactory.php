<?php

namespace Database\Factories;

use App\Models\Lab;
use App\Models\Machine;
use App\Models\TypeMachine;
use Illuminate\Database\Eloquent\Factories\Factory;

class MachineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Machine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'id_type' => TypeMachine::factory()->make()->id,
            'id_lab' => Lab::factory()->make()->id,
            'bridged' => $this->faker->boolean(),
            'default_route' => null,
        ];
    }
}
