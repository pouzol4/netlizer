<?php

namespace Database\Factories;

use App\Models\Machine;
use App\Models\Network;
use App\Models\NetworkInterface;
use App\Models\TypeInterface;
use Illuminate\Database\Eloquent\Factories\Factory;

class NetworkInterfaceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NetworkInterface::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_machine' => Machine::factory()->make()->id,
            'id_network' => Network::factory()->make()->id,
            'id_type' => TypeInterface::factory()->make()->id,
        ];
    }
}
