<?php

namespace Database\Factories;

use App\Models\TypeMachine;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeMachineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TypeMachine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
        ];
    }
}
