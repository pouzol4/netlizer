<?php

namespace Database\Factories;

use App\Models\Lab;
use App\Models\Network;
use Illuminate\Database\Eloquent\Factories\Factory;

class NetworkFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Network::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'mask' => '/24',
            'name' => $this->faker->word(),
            'id_lab' => Lab::factory()->make()->id,
        ];
    }
}
