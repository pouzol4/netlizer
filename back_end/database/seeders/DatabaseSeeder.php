<?php

namespace Database\Seeders;

use App\Models\Network;
use App\Models\Ip;
use App\Models\Lab;
use App\Models\Machine;
use App\Models\NetworkInterface;
use App\Models\TypeInterface;
use App\Models\TypeMachine;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        NetworkInterface::truncate();
        TypeInterface::truncate();
        Network::truncate();
        Ip::truncate();
        Lab::truncate();
        Machine::truncate();
        TypeMachine::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(ConstanteSeeder::class);

        Lab::factory()->create(['id' => '1', 'name' => 'TP 1']);

        Network::factory()->create(['id' => '1', 'mask' => '', 'name' => 'public']);
        Network::factory()->create(['id' => '2', 'mask' => '/24', 'name' => 'net0', 'id_lab' => '1', 'ip' => '192.168.1.0']);
        Network::factory()->create(['id' => '3', 'mask' => '/24', 'name' => 'net1', 'id_lab' => '1', 'ip' => '172.16.5.0']);
        Network::factory()->create(['id' => '4', 'mask' => '/24', 'name' => 'net2', 'id_lab' => '1', 'ip' => '172.16.7.0']);


        Machine::factory()->create(['id' => '1', 'name' => 'R1', 'id_type' => '2', 'id_lab' => '1', 'bridged' => true]);
        NetworkInterface::factory()->create(['id' => '1', 'id_machine' => '1', 'id_network' => '3', 'id_type' => '1']);
        NetworkInterface::factory()->create(['id' => '2', 'id_machine' => '1', 'id_network' => '4', 'id_type' => '2']);

        Machine::factory()->create(['id' => '2', 'name' => 'R0', 'id_type' => '2', 'id_lab' => '1', 'bridged' => false, 'default_route' => '1']);
        NetworkInterface::factory()->create(['id' => '3', 'id_machine' => '2', 'id_network' => '2', 'id_type' => '1']);
        NetworkInterface::factory()->create(['id' => '4', 'id_machine' => '2', 'id_network' => '3', 'id_type' => '2']);

        Machine::factory()->create(['id' => '3', 'name' => 'PCA', 'id_type' => '1', 'id_lab' => '1', 'bridged' => false, 'default_route' => '3']);
        NetworkInterface::factory()->create(['id' => '5', 'id_machine' => '3', 'id_network' => '2', 'id_type' => '1']);

        Machine::factory()->create(['id' => '4', 'name' => 'PCB', 'id_type' => '1', 'id_lab' => '1', 'bridged' => false, 'default_route' => '3']);
        NetworkInterface::factory()->create(['id' => '6', 'id_machine' => '4', 'id_network' => '2', 'id_type' => '1']);

        Machine::factory()->create(['id' => '5', 'name' => 'PCC', 'id_type' => '1', 'id_lab' => '1', 'bridged' => false, 'default_route' => '1']);
        NetworkInterface::factory()->create(['id' => '7', 'id_machine' => '5', 'id_network' => '3', 'id_type' => '1']);

        Machine::factory()->create(['id' => '6', 'name' => 'PCD', 'id_type' => '1', 'id_lab' => '1', 'bridged' => false, 'default_route' => '2']);
        NetworkInterface::factory()->create(['id' => '8', 'id_machine' => '6', 'id_network' => '4', 'id_type' => '1']);

        Ip::factory()->create(['id' => '1', 'value' => '192.168.1.254', 'isIPV6' => false, 'id_interface' => '1']);
        Ip::factory()->create(['id' => '2', 'value' => '192.168.1.1', 'isIPV6' => false, 'id_interface' => '2']);
        Ip::factory()->create(['id' => '3', 'value' => '192.168.1.2', 'isIPV6' => false, 'id_interface' => '3']);
        Ip::factory()->create(['id' => '4', 'value' => '172.16.5.253', 'isIPV6' => false, 'id_interface' => '4']);
        Ip::factory()->create(['id' => '5', 'value' => '172.16.5.254', 'isIPV6' => false, 'id_interface' => '5']);
        Ip::factory()->create(['id' => '6', 'value' => '172.16.5.1', 'isIPV6' => false, 'id_interface' => '6']);
        Ip::factory()->create(['id' => '7', 'value' => '172.16.7.254', 'isIPV6' => false, 'id_interface' => '7']);
        Ip::factory()->create(['id' => '8', 'value' => '172.16.7.1', 'isIPV6' => false, 'id_interface' => '8']);


    }
}
