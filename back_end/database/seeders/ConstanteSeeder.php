<?php

namespace Database\Seeders;

use App\Models\TypeInterface;
use App\Models\TypeMachine;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConstanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        TypeInterface::truncate();
        TypeMachine::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        TypeMachine::insert([
            ['id' => TypeMachine::COMPUTER, 'name' => TypeMachine::NAME[TypeMachine::COMPUTER]],
            ['id' => TypeMachine::ROUTER, 'name' => TypeMachine::NAME[TypeMachine::ROUTER]],
        ]);

        TypeInterface::insert([
            ['id' => TypeInterface::ETH0, 'name' => TypeInterface::NAME[TypeInterface::ETH0]],
            ['id' => TypeInterface::ETH1, 'name' => TypeInterface::NAME[TypeInterface::ETH1]],
            ['id' => TypeInterface::ETH2, 'name' => TypeInterface::NAME[TypeInterface::ETH2]],
            ['id' => TypeInterface::ETH3, 'name' => TypeInterface::NAME[TypeInterface::ETH3]],
        ]);
    }
}
