<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworkInterfaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_interfaces', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_machine');
            $table->foreign('id_machine')
                ->references('id')
                ->on('machines')
                ->onDelete('cascade');

            $table->unsignedBigInteger('id_network')->nullable();
            $table->foreign('id_network')
                ->references('id')
                ->on('networks')
                ->onDelete('cascade');

            $table->unsignedBigInteger('id_type')->nullable();
            $table->foreign('id_type')
                ->references('id')
                ->on('type_interfaces')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('network_interfaces');
    }
}
