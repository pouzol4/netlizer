<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('id_type');
            $table->foreign('id_type')
                ->references('id')
                ->on('type_machines')
                ->onDelete('cascade');

            $table->unsignedBigInteger('id_lab')->nullable();
            $table->foreign('id_lab')
                ->references('id')
                ->on('labs')
                ->onDelete('cascade');
            $table->boolean('bridged')->default(false);
            $table->boolean('isIPV6')->default(false);
            $table->unsignedBigInteger('default_route')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
