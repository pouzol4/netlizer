<?php

namespace App\Http\Controllers;

use App\Models\Lab;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LabController extends Controller
{
    private $rules = [
        'name' => 'required|max:255'
    ];

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Lab::orderByDesc('updated_at')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request): array
    {
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            $response = [
                'response' => $validator->messages(),
                'success' => false,
            ];
        } else {
            $lab = Lab::create($request->all());

            $response = [
                'response' => $lab->id,
                'success' => true,
            ];
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param Lab $lab
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function show(Lab $lab)
    {
        return Lab::with(
            'networks',
            'machines.type',
            'machines.networkInterfaces.ips',
            'machines.networkInterfaces.network',
            'machines.networkInterfaces.type')
            ->find($lab->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function update(Request $request, int $id): array
    {
        $lab = Lab::find($id);

        if ($lab != null) {
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails()) {
                return [
                    'response' => $validator->messages(),
                    'success' => false,
                ];
            } else {
                $lab->name = $request->name;
                $lab->save();

                return [
                    'response' => $lab->id,
                    'success' => true,
                ];
            }
        } else {
            return [
                'response' => 'The lab does not exist',
                'success' => false
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int
     * @return array
     */
    public function destroy(int $id): array
    {
        if (Lab::find($id) != null) {
            Lab::find($id)->delete();
            return [
                'response' => 'The lab has been removed',
                'success' => true
            ];
        } else {
            return [
                'response' => 'The lab does not exist',
                'success' => false
            ];
        }
    }
}
