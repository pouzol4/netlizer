<?php

namespace App\Http\Controllers;

use App\Models\Ip;
use App\Models\Machine;
use App\Models\Network;
use App\Models\NetworkInterface;
use App\Models\TypeInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MachineController extends Controller
{

    public function rules($labId = 0, $machineId = 0): array

    {
        return [
            'name' => [
                'required',
                'max:255',
                Rule::unique('machines', 'name')->where('id_lab', $labId)->ignore($machineId),
            ],
            'id_type' => 'required|exists:type_machines,id',
            'id_lab' => [
                'required',
            ],
            'bridged' => [
                'sometimes',
                'boolean'
            ],
            'isIPV6' => [
                'sometimes',
                'boolean'
            ],
            'default_route' => [
                'sometimes',
                Rule::exists('machines', 'id')->where('id_lab', $labId)
            ]
        ];
    }

    public function rulesUpdate($labId, $machineId)
    {
        return [
            'name' => [
                'max:255',
                Rule::unique('machines', 'name')->where('id_lab', $labId)->ignore($machineId),
            ],
            'id_type' => 'exists:type_machines,id',
            'id_lab' => 'exists:labs,id',
            'bridged' => [
                'boolean'
            ],
            'isIPV6' => [
                'boolean'
            ],
            'connect_machine' => [
                Rule::exists('machines', 'id')->where('id_lab', $labId)
            ]
        ];
    }

    public function rulesDetailsMachines($labId = 0): array
    {
        return [
            'networkInterface' => [
                'array'
            ],
            'networkInterface.*.domain' => [
                Rule::exists('networks', 'name')->where('id_lab', $labId),
                'required'
            ],
            'networkInterface.*.interface' => [
                'numeric',
                'required'
            ],
            'networkInterface.ips.*.ip' => [
                'required',
                'ip',
            ],
            'networkInterface.ips.*.isIPV6' => [
                'sometimes',
                'boolean'
            ],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request): array
    {
        $validator = Validator::make($request->all(), $this->rules($request['id_lab'] ?? null));

        if ($validator->fails()) {
            $response = [
                'response' => $validator->messages(),
                'success' => false,
            ];
        } else {
            Machine::create($request->all());

            $response = [
                'response' => 'The machine has been added',
                'success' => true,
            ];
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param Machine $machine
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function show(Machine $machine)
    {
        $machineEntity = Machine::with(
            'networkInterfaces.ips',
            'networkInterfaces.type',
            'networkInterfaces.network'
        )
            ->find($machine->id);

        $machineEntity->defaultRouteInterfaces = Machine::with(
            'network_interfaces.machines',
            'network_interfaces.ips',
            'network_interfaces.network',
        )
            ->find($machine->id)
            ->network_interfaces;
        return $machineEntity;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Machine $machine
     * @return array
     */
    public function update(Request $request, Machine $machine): array
    {
        $validator = Validator::make($request->all(), $this->rulesUpdate($request['id_lab'] ?? $machine->id_lab, $machine->id));

        if ($validator->fails()) {
            $response = [
                'response' => $validator->messages(),
                'success' => false,
            ];
        } else {
            $machine->update($request->all());

            $response = [
                'response' => 'The machine has been update',
                'success' => true,
            ];
        }

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array
     */
    public function destroy(int $id): array
    {
        if (Machine::find($id)) {
            Machine::find($id)->delete();
            return [
                'response' => 'The machine has been removed',
                'success' => true
            ];
        } else {
            return [
                'response' => 'The machine does not exist',
                'success' => false
            ];
        }
    }

    public function createComplete(Request $request)
    {
        $request['networkInterface'] = collect($request['networkInterface'])
            ->map(function ($networkInterface) {
                return (array)$networkInterface;
            })
            ->sortBy('interface')
            ->toArray();
        $validator = Validator::make($request->all(), array_merge($this->rules($request['id_lab'], $request['machineId'] ?? 0), $this->rulesDetailsMachines($request['id_lab'])));

        $interfaceWanted = [];
        for ($i = 0; $i < count($request['networkInterface'] ?? []); $i++)
            $interfaceWanted[] = $i;

        $nbrGoodInterface = collect($request['networkInterface'])
            ->pluck('interface')
            ->unique()
            ->intersect($interfaceWanted)
            ->count();

        $errorNetworkInterface = $nbrGoodInterface === count($request['networkInterface'])
            ? []
            : ['networkInterface' => 'We need interface ' . implode(', ', $interfaceWanted)];

        $request['isIPV6'] = collect($request['networkInterface'])->pluck('ips')->pluck('isIPV6')->contains(1)
            ?: ($request['isIPV6'] ?? false);

        if ($validator->fails() || $errorNetworkInterface) {
            $response = [
                'response' => $validator->messages()->merge($errorNetworkInterface),
                'success' => false,
            ];
        } else {
            if ($request['machineId'] ?? false) {
                Machine::where('id', $request['machineId'])->update([
                    'name' => $request['name'],
                    'id_lab' => $request['id_lab'],
                    'id_type' => $request['id_type'],
                    'default_route' => $request['default_route'] ?? null,
                    'bridged' => $request['bridged'] ?? false,
                    'isIPV6' => $request['isIPV6'] ?? false
                ]);
                $machine = Machine::find($request['machineId']);
            } else
                $machine = Machine::where([['name', $request['name']], ['id_lab', $request['id_lab']]])->first()
                    ?? Machine::create($request->all());

            $idsResponse = $request['machineId'] ?? false
                    ? $this->updateNetworkInterfaces($request->all(), $machine)
                    : $this->createNetworkInterfaces($request->all(), $machine);

            $defaultRoute = NetworkInterface::whereIn('id_network', collect($idsResponse)->pluck('network'))
                ->where('id_machine', $request['machine_connect'] ?? 0)
                ->first();

            if ($defaultRoute) {
                $machine->default_route = $defaultRoute->id;
                $machine->save();
            }

            $response = [
                'response' => [
                    'machine' => $machine->id,
                    'networkInterface' => $idsResponse,
                ],
                'success' => true,
            ];
        }
        return $response;
    }

    /**
     * @param $request
     * @param $machine
     * @return Collection
     */
    public function createNetworkInterfaces($request, $machine): Collection
    {
        return collect($request['networkInterface'])
            ->map(function ($networkInterface) use ($request, $machine) {
                return $this->createNetworkInterface($networkInterface, $request['id_lab'], $machine);
            });
    }

    private function updateNetworkInterfaces($request, $machine)
    {
        $networkInterfaceNeedToKeep =
            collect($request['networkInterface'])->map(function ($networkInterface) use ($machine, $request) {
                $getNetworkInterface = NetworkInterface
                    ::whereHas('network', function ($query) use ($networkInterface) {
                        $query->where('name', $networkInterface['domain']);
                    })
                    ->whereHas('type', function ($query) use ($networkInterface) {
                        $query->where('name', 'eth' . $networkInterface['interface']);
                    })
                    ->with('ips')
                    ->where('id_machine', $request['machineId'])
                    ->first();
                if (!$getNetworkInterface)
                    return $this->createNetworkInterface($networkInterface, $request['id_lab'], $machine);
                return $this->idArrayNetworkInterface($getNetworkInterface->id_network, $getNetworkInterface->id, $getNetworkInterface->id_type, $getNetworkInterface->ips->pluck('id'));
            })
                ->filter();


        NetworkInterface::where('id_machine', $request['machineId'])
            ->whereNotIn('id', $networkInterfaceNeedToKeep->pluck('typeInterface'))
            ->delete();

        return $networkInterfaceNeedToKeep->toArray();
    }

    /**
     * @param $networkInterface
     * @param $id_lab
     * @param $machine
     * @return array
     */
    public function createNetworkInterface($networkInterface, $id_lab, $machine): array
    {
        $network = Network::where([['name', $networkInterface['domain']], ['id_lab', $id_lab]])->first();

        $typeInterface = TypeInterface::firstOrCreate([
            'name' => 'eth' . $networkInterface['interface']
        ]);

        $interface = NetworkInterface::firstOrCreate([
            'id_machine' => $machine->id,
            'id_network' => $network->id,
            'id_type' => $typeInterface->id,
        ]);

        $ipsId = collect($networkInterface['ips'])->map(function ($ip) use ($interface) {
            return Ip::firstOrCreate([
                'value' => $ip->ip ?? $ip['ip'],
                'id_interface' => $interface->id,
                'isIPV6' => $ip->isIpV6 ?? $ip['isIpV6'] ?? false,
            ])->id;

        });
        return $this->idArrayNetworkInterface($network->id, $typeInterface->id, $interface->id, $ipsId);
    }

    /**
     * @param $networkId
     * @param $typeInterfaceId
     * @param $interfaceId
     * @param $ipIds
     * @return array
     */
    public function idArrayNetworkInterface($networkId, $typeInterfaceId, $interfaceId, $ipIds): array
    {
        return [
            'network' => $networkId,
            'typeInterface' => $typeInterfaceId,
            'interface' => $interfaceId,
            'ips' => $ipIds,
        ];
    }
}
