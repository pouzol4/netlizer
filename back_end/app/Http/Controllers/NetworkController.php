<?php

namespace App\Http\Controllers;

use App\Models\Network;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\Console\Output\ConsoleOutput;

class NetworkController extends Controller
{
    private function rules($lab_id): array
    {
        return [
            'mask' => 'required|numeric|between:1,31',
            'name' => [
                'required',
                Rule::unique('networks', 'name')->where('id_lab', $lab_id),
                'max:255',
            ],
            'id_lab' => 'required|exists:labs,id',
            'ip' => 'required|ip',
        ];
    }

    private function rulesUpdate($machineId, $lab_id): array
    {
        return [
            'name' => [
                Rule::unique('networks', 'name')->where('id_lab', $lab_id)->ignore($machineId),
                'max:255',
            ],
            'id_lab' => 'exists:labs,id',
            'ip' => 'ip',
        ];
    }

    /**
     * Display a listing of the resource for a specific lab.
     */
    public function indexForSpecificLab($lab_id)
    {
        return Network::with('network_interfaces.machines', 'network_interfaces.ips')
            ->where('id_lab', $lab_id)
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request): array
    {
        $validator = Validator::make($request->all(), $this->rules($request['id_lab'] ?? 0));

        //Verification of the tow parameters
        //If it exists, create the network, else give an excpetion
        if ($validator->fails()) {
            $response = [
                'response' => $validator->messages(),
                'success' => false,
            ];
        } else {
            $network = new Network;
            $network->mask = '/' . $request->mask;
            $network->id_lab = $request->id_lab;
            $network->name = $request->name;
            $mask = $this->getMask($request->mask);
            $network->ip = long2ip( ip2long($request->ip) & ip2long($mask));
            $network->save();

            $response = [
                'response' => $network->id,
                'success' => true,
            ];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function update(Request $request, int $id): array
    {
        $network = Network::find($id);

        if ($network != null) {
            $validator = Validator::make($request->all(), $this->rulesUpdate($id, $request['id_lab'] ?? $network->id_lab));

            if ($validator->fails()) {
                return [
                    'response' => $validator->messages(),
                    'success' => false,
                ];
            } else {
                $network->name = empty($request->name) ? $network->name : $request->name;
                $network->mask = empty($request->mask) ? $network->mask : '/' . $request->mask;
                $network->id_lab = empty($request->id_lab) ? $network->id_lab : $request->id_lab;
                $network->save();

                return [
                    'response' => $network->id,
                    'success' => true,
                ];
            }
        } else {
            return [
                'response' => 'The network does not exist',
                'success' => false
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return array
     */
    public function destroy(int $id): array
    {
        $network = Network::find($id);
        if ($network != null) {
            $network->delete();
            return ['response' => 'The network has been removed', 'success' => true,];
        } else {
            return ['response' => 'The network does not exist', 'success' => false,];
        }
    }

    /**
     * @param int $mask
     * @return array|false
     */
    private function getMask(int $mask)
    {
        $mask_array = str_split(str_repeat("1", $mask) . str_repeat("0", 32-$mask), 8);
        foreach ($mask_array as $index => $eightBit)
            $mask_array[$index] = bindec($eightBit);

        return implode('.', $mask_array);
    }
}
