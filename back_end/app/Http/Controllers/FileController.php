<?php

namespace App\Http\Controllers;

use App\Models\Ip;
use App\Models\Lab;
use App\Models\Machine;
use App\Models\Network;
use App\Models\NetworkInterface;
use App\Models\TypeInterface;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use ZipArchive;

class FileController extends Controller
{
    private string $labPath;

    /**
     * @param int $lab_id
     */
    public function getFilePath(int $lab_id): void
    {
        $this->labPath = $this->labPath ?? 'lab_' . $lab_id . '/';
    }

    /**
     * @param $response
     * @param $success
     * @return array
     */
    public function getResponse($response, $success): array
    {
        return [
            'response' => $response,
            'success' => $success,
        ];
    }

    /**
     * @param int $lab_id
     * @return mixed|BinaryFileResponse
     * @throws FileNotFoundException
     */
    public function generateLabFile(int $lab_id)
    {
        $this->getFilePath($lab_id);
        $this->generateConfFile($lab_id);
        $this->generateAllStartupFile($lab_id);
        $this->generateHostFile($lab_id);
        return $this->zipFolder($this->labPath);
    }

    /**
     * @param int $lab_id
     * @return array
     */
    public function generateConfFile(int $lab_id): array
    {
        $this->getFilePath($lab_id);
        $lab = Lab::with(
            'machines.type',
            'machines.networkInterfaces.network',
            'machines.networkInterfaces.type',
        )
            ->find($lab_id);

        if (empty($lab))
            return $this->getResponse('Lab not found', false);

        $fileContent = $lab->machines
            ->map(function ($machine) {
                return $machine->networkInterfaces
                    ->map(function ($networkInterfaces) use ($machine) {
                        preg_match('/[0-9]+/', $networkInterfaces->type->name, $numInterface);
                        $numInterface = $numInterface[0] ?? null;
                        if ($numInterface != null && $networkInterfaces->network->name != "public") {
                            return $machine->name.'['.$numInterface.']='.$networkInterfaces->network->name;
                        }
                    });
            })
            ->flatten();

        $bridged = $lab->machines
            ->where('bridged', true)
            ->map(function ($machine) {
                return $machine->name . '[bridged]=true';
            })
            ->flatten();

        foreach ($bridged as $line) {
            $fileContent[] = $line;
        }
        foreach($fileContent as $key => $value) {
            if(empty($value))
                unset($fileContent[$key]);
        }

        Storage::disk('public')->put($this->labPath . 'lab.conf', $fileContent->implode("\n"));
        return $this->getResponse($lab_id, true);
    }

    /**
     * @param int $lab_id
     * @return array
     */
    public function generateHostFile(int $lab_id): array
    {
        // get the lab
        $this->getFilePath($lab_id);

        //get machines on this lab
        $machines =  Lab::with('machines.type', 'machines.networkInterfaces.network', 'machines.networkInterfaces.type')
            ->find($lab_id)
            ->machines;

        //create .host file
        Storage::disk('public')->put($this->labPath . '.host', '');

        //get all ips
        $ips = Ip::all();

        //get all ips of all machines
        $machines->each(function($machine) use ($ips) {
            $ips->filter(function ($ip) use ($machine) {
                return $ip->id_machine === $machine->id;
            });
            $ips->each(function($ip) use ($machine) {
                Storage::disk('public')->append($this->labPath . '.host',
                    $ip->value . '  ' . $machine->name);
            });
        });
        return $this->getResponse($lab_id, true);
    }

    /**
     * @param int $lab_id
     * @return array
     */
    public function generateAllStartupFile(int $lab_id): array
    {
        $this->getFilePath($lab_id);
        $lab = Lab::with('machines')->find($lab_id);

        if (empty($lab))
            return $this->getResponse('Lab not found', false);

        $lab->machines
            ->map(function ($machine) {
                $this->generateOneStartupFile($machine->id);
            })
            ->flatten();

        return $this->getResponse($lab_id, true);
    }

    /**
     * @param int $machine_id
     * @return void
     */
    public function generateOneStartupFile(int $machine_id): void
    {
        $this->getFilePath($machine_id);
        $machine = Machine::with(
            'networkInterfaces',
            'networkInterfaces.network',
            'networkInterfaces.ips',
            'networkInterfaces.type',
        )
            ->find($machine_id);

        $fileContent = $machine->networkInterfaces
            ->map(function ($networkInterfaces) {
                return
                    $networkInterfaces->ips
                        ->map(function ($ips) use ($networkInterfaces) {
                            $info[] = "ip address add " . $ips->value.$networkInterfaces->network->mask. " dev " . $networkInterfaces->type->name;
                            $info[] = "ip link set dev " . $networkInterfaces->type->name . " up";
                            return $info;
                        });
            })
            ->flatten();

        $publicAcces = $machine->networkInterfaces
            ->map(function ($networkInterfaces) {
                if ($networkInterfaces->network->name == "public") {
                    return "iptables -t nat -A POSTROUTING -o ".$networkInterfaces->type->name." -j MASQUERADE";
                }
            })
            ->flatten();

        $ip = Ip::with('network_interfaces')
            ->where('id', $machine->default_route)
            ->get();

        if (($ip[0] ?? null) != null) {
            $fileContent[] = "ip route add default via " . $ip[0]->value;
        }

        if ($this->isRouter($machine_id)) {
            $fileContent[] = "echo 1 > /proc/sys/net/ipv4/ip_forward";
        }

        foreach($publicAcces as $key => $value) {
            if(!empty($value))
                $fileContent[] = $value;
        }
        Storage::disk('public')->put($this->labPath . $machine->name . '.startup', $fileContent->implode("\n"));
    }

    /**
     * @param $interface
     * @return Stringable
     */
    public function getNumberInterface($interface): Stringable
    {
        return Str::of($interface->name)->replace(TypeInterface::prefix, '');
    }

    /**
     * @param int $lab_id
     * @return Collection
     */
    public function getNetworkInterfacesNotUseless(int $lab_id): Collection
    {
        return Network::with('network_interfaces')
            ->whereHas('network_interfaces', function ($query) use ($lab_id) {

                $query->whereHas('machines', function ($query) use ($lab_id) {
                    $query->where('id_lab', $lab_id);
                });
            })
            ->get()
            ->filter(function ($network) {
                return count($network->network_interfaces) >= 2;
            })
            ->pluck('network_interfaces')
            ->flatten()
            ->pluck('id');
    }


    /**
     * @throws FileNotFoundException
     */
    function zipFolder(string $folderPath): BinaryFileResponse
    {
        $zip = new ZipArchive;
        $fileName = Str::replace('/', '', $folderPath) . '.zip';

        Storage::makeDirectory('tmp');
        if ($zip->open(storage_path('app/tmp/' . $fileName), ZipArchive::CREATE) === false)
            throw new FileException('We can\'t open the file to be zipped');

        $this->putFilesInZip($folderPath, $zip);
        $zip->close();

        if (!Storage::exists('tmp/' . $fileName))
            throw new FileNotFoundException();


        return response()->download(storage_path('app/tmp/' . $fileName), $fileName, ['Content-Type: application/zip'])->deleteFileAfterSend(true);
    }

    private function putFilesInZip(string $folderPath, ZipArchive $zip)
    {
        $directories = Storage::disk('public')->directories($folderPath);
        $folderInZip = Str::of($folderPath)->replace($this->labPath, '');

        foreach ($directories as $directory) {
            $zip->addEmptyDir($folderInZip);
            $this->putFilesInZip($directory . '/', $zip);
        }

        $files = Storage::disk('public')->files($folderPath);
        foreach ($files as $file)
            $zip->addFile(storage_path() . '/app/public/' . $file, $folderInZip . basename($file));
    }

    /**
     * @param int $lab_id
     * @return array
     */
    public function importZipFolder(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:zip|max:2048'
        ]);

        $regexLab = '/^[^.]+/';
        preg_match($regexLab, $request->file->getClientOriginalName(), $nameLab);
        $nameLab = $nameLab[0] ?? "Default";

        $lab = Lab::create([
            'name' => $nameLab,
        ]);

        $lab_id = $lab->id;

        $fileName = 'lab_' . $lab_id . '.'.$request->file->extension();
        $path = storage_path('app/public/lab_'.$lab_id.'/import');
        $result = $request->file->move($path, $fileName);

        $zip = new ZipArchive;
        if ($zip->open($path.'/'.$fileName) === TRUE) {
            $zip->extractTo($path);
            $zip->close();

            $this->importContentZip($path, $lab_id);
        }
        if (is_dir($path)) {
            $objects = scandir($path);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($path."/".$object) == "dir") rmdir($path."/".$object); else unlink($path."/".$object);
                }
            }
            reset($objects);
            rmdir($path);
        }

        return $this->getResponse($lab_id, true);
    }

    /**
     * @param string $path
     * @return void
     */
    public function importContentZip(string $path, int $lab_id): void
    {
        $scandir = scandir($path);
        $regex = '/\\.[a-zA-Z]+/';

        foreach($scandir as $fichier){
            preg_match($regex, $fichier, $matches);
            if (($matches[0] ?? "") == '.conf') {
                $this->importConfFile(($path.'/'.$fichier), $lab_id);
            }
        }

        $defaultRoutes = array();
        foreach($scandir as $fichier){
            preg_match($regex, $fichier, $matches);
            if (($matches[0] ?? "") == '.startup') {
                $defaultRoutes = $this->importStartupFile(($path.'/'.$fichier), $lab_id, $defaultRoutes);
            }
        }
        foreach ($defaultRoutes as $key=>&$value) {
            $id_machines = $value[0];
            $ipValue = $value[1];

            $ip = Ip::with('network_interfaces')
                ->whereHas('network_interfaces', function ($query) use ($lab_id) {
                    $query->whereHas('machines', function ($query) use ($lab_id) {
                        $query->where('id_lab', $lab_id);
                    });
                })
                ->where('value', $ipValue)
                ->get();

            $id_default_route = $ip[0]->id_interface;
            $machine = Machine::find($id_machines)->update(['default_route' => $id_default_route]);
        }
    }

    /**
     * @param int $lab_id
     * @return array
     */
    public function importConfFile(string $link, int $lab_id)
    {
        $handle = fopen($link, "r");
        if ($handle) {
            $machines = array();
            $networks = array();
            $interfaces = array();
            $bridged = array();
            while (($line = fgets($handle)) !== false) {
                $order   = array("\r\n", "\n", "\r");
                $line = str_replace($order, '', $line);

                $regexMachine = '/^([a-zA-Z]+(\\d[a-zA-Z]*)*)/';
                preg_match($regexMachine, $line, $nameMachine);
                $nameMachine = $nameMachine[0] ?? null;

                $regexNetwork = '/[\w\d]*$/';
                preg_match($regexNetwork, $line, $nameNetwork);
                $nameNetwork = $nameNetwork[0] ?? null;
                if ($nameNetwork != "true") {
                    $regexInterface = '/\\[\\d]/';
                    preg_match($regexInterface, $line, $Interface);
                    preg_match('/[0-9]+/',$Interface[0], $numInterface);
                    $numInterface = $numInterface[0] ?? null;

                    $positionMachine = array_search($nameMachine, $machines);
                    if ($positionMachine === false) {
                        $machines[] = $nameMachine;
                        $positionMachine = array_search($nameMachine, $machines);
                    }

                    $positionNetwork = array_search($nameNetwork, $networks);
                    if ($positionNetwork === false) {
                        $networks[] = $nameNetwork;
                        $positionNetwork = array_search($nameNetwork, $networks);
                    }

                    $interfaces[] = array($positionMachine, $positionNetwork, $numInterface);
                }
                else {
                    $bridged[] = $nameMachine;
                }
            }
            foreach ($networks as $key=>&$value) {
                $network = Network::create([
                    'mask' => 'default',
                    'name' => $value,
                    'id_lab' => $lab_id,
                ]);
                $networks[$key] = $network->id;
            }

            foreach ($machines as $key=>&$value) {
                $machine = Machine::create([
                    'name' => $value,
                    'id_type' => 1,
                    'id_lab' => $lab_id,
                    'bridged' => in_array($value, $bridged),
                ]);
                $machines[$key] = $machine->id;
            }

            foreach ($interfaces as $key=>&$value) {
                $interface = NetworkInterface::create([
                    'id_machine' => $machines[$value[0]],
                    'id_network' => $networks[$value[1]],
                    'id_type' => $this->getIdTypeInterface('eth'.$value[2]),
                ]);
            }
            fclose($handle);
        } else {
            return $this->getResponse($lab_id, false);
        }

        return $this->getResponse($lab_id, true);
    }

    /**
     * @param int $lab_id
     * @return array
     */
    public function importStartupFile(string $link, int $lab_id, array $defaultRoutes) : array
    {
        $handle = fopen($link, "r");

        preg_match('/[a-zA-Z\d]+\\.startup/', $link, $nameMachine);
        preg_match('/^[a-zA-Z\d]+/', $nameMachine[0] ?? null, $nameMachine);
        $nameMachine = $nameMachine[0] ?? null;

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $order   = array("\r\n", "\n", "\r");
                $line = str_replace($order, '', $line);

                $regexIpLine = '/ip address add/';
                if (preg_match($regexIpLine, $line)) {
                    $regexIP = '/\\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\\b/';
                    preg_match($regexIP, $line, $ip);
                    $ip = $ip[0] ?? null;

                    $regexMask = '/\/[0-9]+/';
                    preg_match($regexMask, $line, $mask);
                    $mask = $mask[0] ?? null;

                    $regexInterface = '/eth[0-9]+/';
                    preg_match($regexInterface, $line, $nameInterface);
                    $nameInterface = $nameInterface[0] ?? null;

                    $networkInterface = NetworkInterface::with('machines')
                        ->whereHas('machines', function ($query) use ($lab_id, $nameMachine, $nameInterface) {
                            $query->where('id_lab', $lab_id);
                            $query->where('name', $nameMachine);
                        })
                        ->whereHas('type', function ($query) use ($nameInterface) {
                            $query->where('name', $nameInterface);
                        })
                        ->get();

                    $id_network = $networkInterface[0]->network->id;
                    $id_interface = $networkInterface[0]->id;

                    if ($networkInterface[0]->network->mask == "default") {
                        $network = Network::find($id_network)->update(['mask' => $mask]);
                    }

                    $ip = Ip::create([
                        'value' => $ip,
                        'isIPV6' => 0,
                        'id_interface' => $id_interface,
                    ]);
                }
                $regexRouter = '/echo 1 > \/proc\/sys\/net\/ipv4\/ip_forward/';
                if (preg_match($regexRouter, $line)) {
                    $lab = Lab::with(["machines" => function ($query) use ($nameMachine) {
                        $query->where("name" , $nameMachine);
                    }])->find($lab_id);

                    $machine = Machine::find($lab->machines[0]->id)->update(['id_type' => 2]);
                }
                $regexIpDefault = '/ip route add default via/';
                if (preg_match($regexIpDefault, $line)) {
                    $regexIP = '/\\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\\b/';
                    preg_match($regexIP, $line, $ip);
                    $ip = $ip[0] ?? null;

                    $lab = Lab::with(["machines" => function ($query) use ($nameMachine) {
                        $query->where("name" , $nameMachine);
                    }])->find($lab_id);

                    $lab->machines[0]->id;

                    $defaultRoutes[] = array($lab->machines[0]->id, $ip);
                }
                $regexPublic = '/iptables -t nat -A POSTROUTING -o eth[0-9]+ -j MASQUERADE/';
                if (preg_match($regexPublic, $line)) {
                    $regexInterface = '/eth[0-9]+/';
                    preg_match($regexInterface, $line, $nameInterface);
                    $nameInterface = $nameInterface[0] ?? null;

                    $machine = Machine::with('lab')
                        ->whereHas('lab', function ($query) use ($lab_id) {
                            $query->where('id_lab', $lab_id);
                        })
                        ->where('name', $nameMachine)
                        ->get();

                    $idNetwork = $this->getIdNetwork("public");

                    $interface = NetworkInterface::create([
                        'id_machine' => $machine[0]->id,
                        'id_network' => $idNetwork,
                        'id_type' => $this->getIdTypeInterface($nameInterface),
                    ]);
                }

            }
            fclose($handle);
        } else {
            return $this->getResponse($lab_id, false);
        }

        return $defaultRoutes;
    }

    /**
     * @param string $nameTypeInterface
     * @return int
     */
    public function getIdTypeInterface(string $nameTypeInterface): int
    {
        $typeInterface = TypeInterface::firstOrCreate([
            'name' => $nameTypeInterface
        ]);
        return $typeInterface->id;
    }

    /**
     * @param string $nameTypeInterface
     * @return int
     */
    public function getIdNetwork(string $nameNetwork): int
    {
        $network = Network::where('name', $nameNetwork)->get();
        return $network[0]->id;
    }

    /**
     * @param int $machine_id
     * @return bool
     */
    public function isRouter(int $machine_id): bool
    {
        $machine = Machine::with('type')
            ->whereHas('type', function ($query){
                $query->where('name', "Router");
            })
            ->where('id', $machine_id)
            ->get();

        if (count($machine) != 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
