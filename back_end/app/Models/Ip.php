<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ip extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'value', 'isIPV6', 'id_interface',
    ];

    public function network_interfaces(): BelongsTo
    {
        return $this->belongsTo(NetworkInterface::class, 'id_interface');
    }
}
