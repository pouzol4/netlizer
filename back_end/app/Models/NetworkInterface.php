<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class NetworkInterface extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'id_machine', 'id_network', 'id_type',
    ];

    public function network(): BelongsTo
    {
        return $this->belongsTo(Network::class, 'id_network');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(TypeInterface::class, 'id_type');
    }

    public function machines(): BelongsTo
    {
        return $this->belongsTo(Machine::class, 'id_machine');
    }

    public function machines_connecte(): HasMany
    {
        return $this->hasMany(Machine::class, 'default_route');
    }

    public function ips(): HasMany
    {
        return $this->hasMany(Ip::class, 'id_interface');
    }
}
