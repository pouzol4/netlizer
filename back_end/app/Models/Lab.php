<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static orderByDesc(string $string)
 * @method static create(array $all)
 * @method static find(int $id)
 *
 * @property mixed id
 */
class Lab extends Model
{
    use HasFactory;

    public $fillable = [
        'name'
    ];

    /**
     * @return HasMany
     */
    public function machines(): HasMany
    {
        return $this->hasMany(Machine::class, 'id_lab');
    }

    /**
     * @return HasMany
     */
    public function networks(): HasMany
    {
        return $this->hasMany(Network::class, 'id_lab');
    }
}
