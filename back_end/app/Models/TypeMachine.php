<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class TypeMachine extends Model
{
    use HasFactory;

    const COMPUTER = 1;
    const ROUTER = 2;
    const SWITCH = 3;
    const HUB = 4;
    const DNS = 5;
    const INTERNET_NETWORK = 6;

    const NAME = [
        self::COMPUTER => "Computer",
        self::ROUTER => "Router",
        self::SWITCH => "Switch",
        self::HUB => "Hub",
        self::DNS => "DNS",
        self::INTERNET_NETWORK => "Internet Network",
    ];

    public $timestamps = false;
}
