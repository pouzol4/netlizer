<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeInterface extends Model
{
    use HasFactory;

    const ETH0 = 1;
    const ETH1 = 2;
    const ETH2 = 3;
    const ETH3 = 4;

    const NAME = [
        self::ETH0 => 'eth0',
        self::ETH1 => 'eth1',
        self::ETH2 => 'eth2',
        self::ETH3 => 'eth3',
    ];
    const prefix = 'eth';

    public $fillable = ['name'];

    public $timestamps = false;
}
