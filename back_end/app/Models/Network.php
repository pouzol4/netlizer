<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Network extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $fillable = [
        'mask',
        'name',
        'id_lab',
    ];

    public function network_interfaces(): HasMany
    {
        return $this->hasMany(NetworkInterface::class, 'id_network');
    }
}
