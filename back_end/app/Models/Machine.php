<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Machine extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name', 'id_type', 'id_lab', 'bridged', 'isIPV6', 'default_route',
    ];

    public function type(): BelongsTo
    {
        return $this->belongsTo(TypeMachine::class, 'id_type');
    }

    public function lab(): BelongsTo
    {
        return $this->belongsTo(Lab::class, 'id_lab');
    }

    public function network_interfaces(): BelongsTo
    {
        return $this->belongsTo(NetworkInterface::class, 'default_route');
    }

    public function networkInterfaces(): HasMany
    {
        return $this->hasMany(NetworkInterface::class, 'id_machine');
    }
}
