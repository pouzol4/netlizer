<?php

use App\Http\Controllers\FileController;
use App\Http\Controllers\NetworkController;
use App\Http\Controllers\LabController;
use App\Http\Controllers\MachineController;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Console\Output\ConsoleOutput;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

(new ConsoleOutput())->writeln("Url : ".Request::url()."\nMethod : ".Request::method()."\nParams : ".json_encode(Request::all()));

Route::apiResource('labs', LabController::class);
Route::apiResource('machines', MachineController::class);
Route::apiResource('networks', NetworkController::class);
Route::get('/networks/labs/{lab_id}', [NetworkController::class, 'indexForSpecificLab']);
Route::get('/lab/{lab_id}/files/all', [FileController::class, 'generateLabFile']);
Route::get('/lab/{lab_id}/files/conf', [FileController::class, 'generateConfFile']);
Route::get('/lab/{lab_id}/files/host', [FileController::class, 'generateHostFile']);
Route::post('/machines/complete-create', [MachineController::class, 'createComplete']);
Route::get('/lab/{lab_id}/files/startup', [FileController::class, 'generateAllStartupFile']);

Route::post('/lab/new/files/import/zip', [FileController::class, 'importZipFolder']);
Route::post('/lab/{lab_id}/files/import/conf', [FileController::class, 'importConfFile']);
