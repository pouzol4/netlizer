import $ from "jquery"
import "jquery-ui-dist/jquery-ui"

$(document).on("dblclick", "#content", function () {
    let current = $(this).text()
    $("#content").html('<input type="text" class="form-control" id="newcont" value="'+current+'"/>')

    let newContentEl = $("#newcont")
    newContentEl.css('font-size', $(this).css('font-size'))
    newContentEl.focus();

    newContentEl.focus(function () {
        console.log('in')
    }).blur(function () {
        let newContentText = newContentEl.val()
        if (newContentText === "") {
            newContentText = current
        }
        $("#content").text(newContentText)
    })

})