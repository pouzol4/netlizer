import React, {useState, useEffect} from 'react'

export class MachineList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pathMachineImg: props.pathMachineImg,
        }

    }

    onDragStart = (event, data) => {
        event.dataTransfer.setData('application/reactflow', data)
        event.dataTransfer.effectAllowed = 'move'
    }

    render() {
        return (
            <aside className="machineList">
                <div className="dndnode" onDragStart={(event) => this.onDragStart(event, JSON.stringify({src: 'computer.png', type: 1}))} draggable>
                    <figure><img id="img1" className="img-source" src={this.state.pathMachineImg + "computer.png"} alt="computer.png"/>
                        <figcaption>PC</figcaption>
                    </figure>
                </div>


                <div className="dndnode" onDragStart={(event) => this.onDragStart(event, JSON.stringify({src: 'router.png', type: 2}))} draggable>
                    <figure><img id="img4" className="img-source" src={this.state.pathMachineImg + "router.png"} alt="router.png"/>
                        <figcaption>Router</figcaption>
                    </figure>
                </div>

            </aside>
        );
    }
}
