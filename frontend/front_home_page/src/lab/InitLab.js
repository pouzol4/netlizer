import React from 'react';

const onDragStartTarget = (e) => {
    e.preventDefault();
};

export const initLab = function (data) {
    const pathMachineImg = '/assets/machines/'

    let initialElements = []
    let interfaces = []
    let edges = []
    let domains = []

    for (let i = 0; i <= data['machines'].length - 1; i++) {
        let machine = data['machines'][i]
        let x = Math.random() * (0 - 500) + 500
        let y = Math.random() * (0 - 500) + 500
        for (let j = 0; j <= machine.network_interfaces.length - 1; j++) {
            let domain = machine.network_interfaces[j].network.name.replace('net', '')
            interfaces.push({
                idMachine: machine.id,
                interface: machine.network_interfaces[j].type.name.replace('eth', ''),
                domain: domain,
                ipAddress: machine.network_interfaces[j].ips.value
            })
            if (!domains.includes(domain)) {
                domains.push(domain)
            }
        }
        initialElements.push({
            id: `dndnode_` + machine.id,
            name: machine.name,
            interfaces: interfaces,
            type: 'default',
            typeMachine: machine.type.name,
            position: {x: x, y: y},
            data: {
                label: (
                    <>
                        <img data-id={`dndnode_` + machine.id} className="img-target" src={pathMachineImg + machine.type.name.toLowerCase() + ".png" }
                             onDragStart={(e) => onDragStartTarget(e)} alt={machine.type.name.toLowerCase()} data-type={machine.type.id}/>
                    </>
                ),
            },
        })
    }

    for (let k = 0; k <= domains.length - 1; k++) {
        let newEdges = interfaces.filter(el => el.domain === domains[k])
        for (let m = 0; m <= newEdges.length - 1; m++) {
            let source = newEdges[m]
            for (let n = 0; n <= newEdges.length - 1; n++) {
                let target = newEdges[n]
                if (source.idMachine !== target.idMachine) {
                    let e = {
                        id: 'dndnode_' + source.idMachine + '-dndnode_' + target.idMachine,
                        source: 'dndnode_' + source.idMachine,
                        target: 'dndnode_' + target.idMachine,
                        label: 'eth' + source.interface + ' - ' + 'eth' + target.interface
                    }
                    let e2 = {
                        id: 'dndnode_' + target.idMachine + '-dndnode_' + source.idMachine,
                        source: 'dndnode_' + target.idMachine,
                        target: 'dndnode_' + source.idMachine,
                        label: 'eth' + target.interface + ' - ' + 'eth' + source.interface
                    }
                    // Avoid double edge
                    // Example : id1 link to id2 edge existed so we don't need id2 link to id1
                    if (!initialElements.some(function (el) {
                        return el.id === e2.id;
                    })) {
                        initialElements.push(e)
                    }
                }
            }
        }
    }
    return initialElements
}
