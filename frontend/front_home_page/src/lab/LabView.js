import React, {useState, useRef} from 'react'
import {InterfacesChoiceModalForm} from "../form/InterfacesChoiceModalForm";
import {LabReactFlow} from "./LabReactFlow";

export class LabView extends React.Component {

    render() {
        return (
            <div className="lab-view">
                <LabReactFlow lab={this.props.lab} name={this.props.name}/>
            </div>
        )
    }
}
