import React, {useRef, useState} from 'react'
import ReactFlow, {
    addEdge,
    Controls,
    ReactFlowProvider,
    removeElements,
    updateEdge,
} from 'react-flow-renderer'
import {MachineList} from '../machine/MachineList'
import {MachineConfigurationForm} from '../form/MachineConfigurationForm'
import {DomainConfigurationForm} from '../form/DomainConfigurationForm'
import {InterfacesChoiceModalForm} from '../form/InterfacesChoiceModalForm'
import {LabNameForm} from '../form/LabNameForm'
import {initLab} from './InitLab'
import Dataprovider from '../dataprovider/Dataprovider.js'

// Import Link from react-router-dom
// Used to create a link to other pages
import {Link} from 'react-router-dom'

const pathMachineImg = '/assets/machines/'
const pathLogoImg = '/assets/logo/'

export const LabReactFlow = (props) => {
    let classDomainIsActive = false
    // React Flow Variables
    const reactFlowWrapper = useRef(null)
    const [reactFlowInstance, setReactFlowInstance] = useState(null)
    const [elements, setElements] = useState(initLab(props.lab))

    // Modal Variables
    const [modalIsHidden, setModalIsHidden] = useState(true)
    const [machineConfigIsHidden, setMachineConfigIsHidden] = useState(true)
    const [networkConfigIsHidden, setNetworkConfigIsHidden] = useState(false)
    const [machineConfigId, setMachineConfigId] = useState(null)
    const [machineConfigType, setMachineConfigType] = useState(null)
    const [interfacesChoiceIsHidden, setInterfacesChoiceIsHidden] = useState(true)
    const [interfacesList1, setInterfacesList1] = useState([])
    const [interfacesList2, setInterfacesList2] = useState([])


    /******************** React Flow functions *************/
    const onConnect = (params) => {
        let machine1 = elements.find(element => element.id === params.source)
        let machine2 = elements.find(element => element.id === params.target)
        if (machine1.interfaces.length === 0 || machine2.interfaces.length === 0) {
            window.alert('One of the 2 machines or both does not have interface(s) to connect between them.')
        } else {
            setModalIsHidden(false)
            let interfacesList = []
            machine1.interfaces.forEach(function (item) {
                if (!interfacesList.some(function (el) {
                    return el.interface === item.interface;
                })) {
                    interfacesList.push(item)
                }
            })
            setInterfacesList1(interfacesList)
            let interfacesList2 = []
            machine2.interfaces.forEach(function (item) {
                if (!interfacesList2.some(function (el) {
                    return el.interface === item.interface;
                })) {
                    interfacesList2.push(item)
                }
            })
            setInterfacesList2(interfacesList2)

            setElements((els) => addEdge(params, els))
        }
    }

    const onDragStartTarget = (e) => {
        e.preventDefault();
    };


    const onElementsRemove = (elementsToRemove) => {
        if (window.confirm('Are you sure to delete the element ? All the data linked to it will be deleted.') === true) {
            let machineId = Number(elementsToRemove[0].id.replace(/[^\d]/g, ""));
            setModalIsHidden(true)
            setElements((els) => removeElements(elementsToRemove, els))
            let isMachine = !elementsToRemove[0].source
            if (!machineConfigIsHidden && isMachine) {
                setMachineConfigIsHidden(true)
                setNetworkConfigIsHidden(false)
            }

            let dataProvider = new Dataprovider();
            dataProvider.deleteMachineFromAPI(machineId)
        }
    }

    const onEdgeUpdate = (oldEdge, newConnection) => {
        if (window.confirm('Are you sure to change the edge ? All interfaces config link to the edge will be deleted.') === true) {
            setElements((els) => updateEdge(oldEdge, newConnection, els))
        }
    }

    const onLoad = (_reactFlowInstance) =>
        setReactFlowInstance(_reactFlowInstance)

    const onDragOver = (event) => {
        event.preventDefault()
        event.dataTransfer.dropEffect = 'move'
    }

    const onDrop = (event) => {
        const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect()
        const data = JSON.parse(event.dataTransfer.getData('application/reactflow'))
        const image = data.src;
        const position = reactFlowInstance.project({
            x: event.clientX - reactFlowBounds.left,
            y: event.clientY - reactFlowBounds.top,
        });
        const inc = elements.length + 1
        const newNode = {
            id: `dndnode_${inc}`,
            name: '',
            interfaces: [{interface: 0, domain: '', ipAddress: ''}],
            type: 'default',
            typeMachine: '',
            position,
            data: {
                label: <img className="img-target" src={pathMachineImg + image} data-type={data.type}
                            onDragStart={(e) => onDragStartTarget(e)} alt={image}/>
            },
        }

        setElements((es) => es.concat(newNode))

        setMachineConfigType(data.type)
        if (machineConfigIsHidden && !classDomainIsActive) {
            setMachineConfigIsHidden(false)
            setNetworkConfigIsHidden(true)
        }
    }

    const onNodeDoubleClick = (event) => {
        try {
            if (event.target.attributes['data-id'] != undefined) {
                const value = event.target.attributes['data-id'].value.match(/[0-9]+/i)[0];
                setMachineConfigId(value)
            } else {
                setMachineConfigId(-1)
            }

            /** @todo not work sometimes */
            let type = event.target.dataset.type;
            if(type === undefined || type === null){
                type = event.target.querySelector("img").dataset.type;
            }
            setMachineConfigType(type)
        } catch (error) {
            console.error(error)
            setMachineConfigId(-1)
        }
        if (machineConfigIsHidden && !classDomainIsActive) {
            setMachineConfigIsHidden(false)
            setNetworkConfigIsHidden(true)
        }
    }

    /******************** End React Flow functions *************/


    /******************** Modal functions *****************/
    const removeForm = () => {
        setInterfacesChoiceIsHidden(false)
    }

    const onChangeMenu = (event) => {
        event.preventDefault()
        const isActive = event.target.classList.contains("config_active")

        if (!isActive) {
            let elems = document.querySelectorAll(".tabs_selector");

            [].forEach.call(elems, function (el) {
                el.classList.remove("config_active")
            })

            event.target.classList.add("config_active")
        }

        classDomainIsActive = event.target.classList.contains("tabs_selector_domains")
        if (classDomainIsActive) {
            setMachineConfigIsHidden(true)
            setNetworkConfigIsHidden(false)
        } else {
            setMachineConfigIsHidden(false)
            setNetworkConfigIsHidden(true)

        }
    }


    return (
        <div>
            <header className="header">
                <Link to={'/'}>
                    <div className="header__logo">
                        <img
                            src={pathLogoImg + 'netlizer_logo_only_removebg.png'}
                            alt="netlizer_logo_only_removebg"
                        />
                        <img
                            src={pathLogoImg + 'netlizer_logo-name_removebg.png'}
                            alt="netlizer_logo-name_removebg"
                        />
                    </div>
                </Link>
                <LabNameForm lab={props.lab}/>
                <ul className="actions-list">
                    <li>
            <span
                className="actions-list__tip"
                data-toggle="tooltip"
                data-placement="bottom"
                title="Help"
                onClick={function () {
                    window.alert(
                        "Key return to delete machine.\nDrag edge to change connection.\nDouble-click to show machine's configuration.",
                    )
                }}
            >
              ❓
            </span>
                    </li>
                    <li>
                        <button className="submit_button">Download</button>
                    </li>
                </ul>
            </header>
            <main class="labview_container">
                <ReactFlowProvider>
                    <MachineList pathMachineImg={pathMachineImg}/>

                    <div className="reactflow-wrapper labView" ref={reactFlowWrapper}>
                        <ReactFlow
                            elements={elements}
                            onConnect={onConnect}
                            onElementsRemove={onElementsRemove}
                            onLoad={onLoad}
                            onDrop={onDrop}
                            onDragOver={onDragOver}
                            onNodeDoubleClick={onNodeDoubleClick}
                            onEdgeUpdate={onEdgeUpdate}
                        >
                            <Controls className="controls_bar"/>
                        </ReactFlow>
                    </div>
                </ReactFlowProvider>

                <aside className="configuration">
                    <ul className="tabs_list">
                        <li
                            className="tabs_selector config_active tabs_selector_machine"
                            onClick={onChangeMenu}
                        >
                            Machine
                        </li>
                        <li
                            className="tabs_selector tabs_selector_domains"
                            onClick={onChangeMenu}
                        >
                            Domains
                        </li>
                    </ul>
                    <div className="config-info">
                        <MachineConfigurationForm id_lab={props.lab.id} isHidden={machineConfigIsHidden}
                                                  idMachine={machineConfigId} id_type={machineConfigType}/>
                        <DomainConfigurationForm id_lab={props.lab.id} isHidden={networkConfigIsHidden}
                                                 networks={props.lab.networks}/>
                        <div id="interfaces-choice" hidden={interfacesChoiceIsHidden}>
                            <form action="" className="">
                                <div
                                    id="interfaces-choice-form"
                                    className="d-flex justify-content-between flex-column"
                                />
                                <button className="btn" type="button" onClick={removeForm}>
                                    Save
                                </button>
                            </form>
                        </div>
                    </div>
                </aside>
            </main>

            <InterfacesChoiceModalForm isHidden={modalIsHidden}
                                       interfacesList1={interfacesList1}
                                       interfacesList2={interfacesList2}/>

        </div>
    )
}
