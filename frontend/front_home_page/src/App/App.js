// Import the style sheet
import './App.css'
// Import React
import React, { useState, useEffect } from 'react'
// Import FrontPage Component
import { FrontPage } from '../Components/FrontPage'
// Import Link, BrowserRouter, Route, Routes, useParams from react-router-dom
// These components are used to create Routes on the App
import { BrowserRouter, Route, Routes, useParams, Link } from 'react-router-dom'

import { LabView } from '../lab/LabView'
import Dataprovider from '../dataprovider/Dataprovider.js'
import UploadFileSys from "../Components/UploadFileSys";
function App() {
  return (
    <BrowserRouter>
      <div className="main-route-place">
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/labs/:id" element={<LabsViewer />} />
            <Route path="/upload" element={<FileUpload />} />
        </Routes>
      </div>
    </BrowserRouter>
  )
}

// Main Page
function Home() {
  
  document.title = "Netlizer Homepage"
  return (
    <div>
        <Link to={'/'}>
          <header>
            <img width="130" class="logo" src="/assets/logo/logo.png" alt="Logo" />
          </header>
        </Link>
      <main>
        <div>
          <FrontPage />
        </div>
        {process.env.REACT_APP_NOT_SECRET_CODE}
      </main>
      <footer></footer>
    </div>
  )
}

// Main Page
function FileUpload() {

    document.title = "Netlizer File Upload"
    return (
        <div>
            <Link to={'/'}>
                <header>
                    <img width="130" class="logo" src="/assets/logo/logo.png" alt="Logo" />
                </header>
            </Link>
            <main>
                <div>
                    <UploadFileSys />
                </div>
                {process.env.REACT_APP_NOT_SECRET_CODE}
            </main>
            <footer></footer>
        </div>
    )
}

// Lab Viewer
function LabsViewer() {
  let params = useParams()
  // Instantiate a dataprovider
  let _dataprovider = new Dataprovider()

  // Create a loading state and fetch from API from Dataprovider
  const [isLoaded, setIsLoaded] = useState(false)
  const [error, setError] = useState(null)
  const [lab, setLab] = useState({})
  useEffect(() => {
    _dataprovider.fetchLabFromAPI(params.id).then(
      (result) => {
        setLab(result)
        setIsLoaded(true)
      },
      (error) => {
          setError(error)
        setIsLoaded(true)
      },
    )
  }, [])

  console.log(error)

  // If the lab is not loaded, show a loading screen
  if (!isLoaded) {
    document.title = "Loading..."
    return (
      <div class="test_container">
        <div>
          <h2>Loading...</h2>
        </div>
      </div>
    )
  } else if (error) {
    document.title = "Error 404"
    return (
      <div class="test_container">
        <div class="error_container">
          <img src='/assets/error/404.png'></img>
          <Link to="/">Back to lab list</Link>
        </div>
      </div>
    )
  } else {
    document.title = "Netlizer - " + lab.name
    return (
      <div>
        <LabView lab={lab} name={lab.name} />
      </div>
    )
  }
}

export default App
