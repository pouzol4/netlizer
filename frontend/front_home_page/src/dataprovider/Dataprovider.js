import React from 'react'

export default class DataProvider extends React.Component {
  // Initialize the state with 'BaseURL' and 'error'
  constructor(props) {
    super(props)
    this.state = {
      baseURL: process.env.REACT_APP_BACKEND_URL,
    }
  }

  // Function to set the BaseURL
  setBaseURLFunc(value) {
    this.setState({
      baseURL: value,
    })
  }

  // Fetch from the API if the labs is defined
  fetchLabFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
      success: false,
    }

    return fetch(this.state.baseURL + '/api/labs/' + id).then((res) =>
      res.json(),
    )
  }

  // Fetch from the API if the machine is defined
  fetchMachineFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
      success: false,
    }

    return fetch(this.state.baseURL + '/api/machines/' + id).then((res) =>
        res.json(),
    )
  }

  // Fetch from the API if the info is defined
  fetchInfoFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
      success: false,
    }

    return fetch(this.state.baseURL + '/api/networks/labs/' + id).then((res) =>
        res.json(),
    )
  }

  // Delete lab from the API
  deleteLabFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'DELETE',
    }

    return fetch(this.state.baseURL + '/api/labs/' + id, requestOptions)
      .then((res) => res.json())
      .then(
        (result) => {
          resultat.result = result
        },
        (error) => {
          resultat.error = error
        },
      )
  }

  postNetworksToAPI(network) {
    // New Object with result and error
    // Need an object with :
    // - id
    // - lab_ID
    // - name
    // - mask (/24)
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(network),
    }

    return fetch(
      this.state.baseURL + '/api/networks',
      requestOptions,
    ).then((response) => response.json())
  }

  // GET Function to get Network
  getNetworksFromLabToAPI(lab_id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'GET',
    }

    return fetch(
      this.state.baseURL + '/api/networks/labs/' + lab_id,
      requestOptions,
    ).then((response) => response.json())
  }

  // Function to modify a Network based on its id
  // Need an object with :
  // - id
  // - lab_ID
  // - name
  // - mask (/24)
  modifyNetworkToAPI(network) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(network),
    }

    return fetch(
      this.state.baseURL + '/api/networks/' + network.id,
      requestOptions,
    ).then((response) => response.json())
  }

  // Function to delete a network based on its id
  // Need :
  // - id
  deleteNetworkFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'DELETE',
    }

    return fetch(this.state.baseURL + '/api/networks/' + id, requestOptions)
      .then((response) => response.json())
      .then(
        (result) => {
          resultat.result = result
        },
        (error) => {
          resultat.error = error
        },
      )
  }

  // Post Function to create a new machine
  // Need an object with :
  // - domain
  // - interface
  // - ip
  // - isIPV6
  postMachineToAPI(machine) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(machine),
    }

    return fetch(
      this.state.baseURL + '/api/machines/complete-create',
      requestOptions,
    ).then((response) => response.json())
  }

  // Post Function to create a new machine thanks to a zip file in parameter
  // Need an object with :
  // - zip
  postMachineFromZipToAPI(file) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(file),
    }

    return fetch(
      this.state.baseURL + '/api/lab/new/files/import/zip',
      requestOptions,
    ).then((response) => response.json())
  }

  // Function to modify a machine based on its id
  // Need an object with :
  // - id
  // - domain
  // - interface
  // - ip
  // - isIPV6
  ModifyMachineToAPI(machine) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(machine),
    }

    return fetch(
      this.state.baseURL + '/api/machines/' + machine.id,
      requestOptions,
    ).then((response) => response.json())
  }

  // Function to delete a machine based on its id
  // Need :
  // - id
  deleteMachineFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'DELETE',
    }

    return fetch(this.state.baseURL + '/api/machines/' + id, requestOptions)
      .then((response) => response.json())
      .then(
        (result) => {
          resultat.result = result
        },
        (error) => {
          resultat.error = error
        },
      )
  }

  // Function to modify a Lab based on its id
  // Need an object with :
  // - id
  // - name
  modifyLabToAPI(lab, id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(lab),
    }

    return fetch(
      this.state.baseURL + '/api/labs/' + id,
      requestOptions,
    ).then((response) => response.json())
  }

  // Function to download all files from a lab
  downloadLabFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'GET',
    }

    window.open(this.state.baseURL + '/api/lab/' + id + '/files/all')
  }

  // Function to download Conf file from a lab
  downloadConfFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'GET',
    }

    window.open(this.state.baseURL + '/api/lab/' + id + '/files/conf')
  }

  // Function to download Host file from a lab
  downloadHostFromAPI(id) {
    // New Object with result and error
    const resultat = {
      result: [],
      error: null,
    }

    const requestOptions = {
      method: 'GET',
    }

    window.open(this.state.baseURL + '/api/lab/' + id + '/files/host')
  }
}
