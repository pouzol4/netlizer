import React, {useState, useEffect} from 'react'
import Dataprovider from '../dataprovider/Dataprovider.js'

export class DomainConfigurationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isHidden: false,
            networks: props.networks,
            name: '',
            ip: '',
            id_lab: this.props.id_lab,
            mask: 1,
            networkUpdateId: 0,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.isHidden !== this.props.isHidden) {
            this.setState({
                isHidden: this.props.isHidden
            });
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        const ip = target.ip;

        this.setState({
            [name]: value,
        });
    }

    handleSubmit(event) {
        let _dataprovider = new Dataprovider()
        if (this.state.networkUpdateId === 0) {
            _dataprovider.postNetworksToAPI(this.getNetwork()).then(
                (result) => {
                    if (result.success === false) {
                        alert(Object.values(result.response)[0]);
                    } else {
                        window.location.reload(false)
                    }
                },
                (error) => {
                    alert(error)
                },
            )
        } else {
            _dataprovider.modifyNetworkToAPI(this.getNetwork()).then(
                (result) => {
                    if (result.success === false) {
                        alert(Object.values(result.response)[0]);
                    } else {
                        window.location.reload(false)
                    }
                },
                (error) => {
                    console.log(error)
                },
            )
        }
        event.preventDefault();
    }

    refreshNetworks() {
        let dataprovider = new Dataprovider();
        dataprovider.getNetworksFromLabToAPI(this.state.id_lab).then(
            (result) => {
                this.setState({networks: result});
            },
            (error) => {
                console.log(error)
            },
        )
    }

    refreshForm() {
        this.setState({
            name: '',
            mask: '',
            networkUpdateId: 0,
        });
    }

    getNetwork() {
        return {
            id: this.state.networkUpdateId,
            name: this.state.name,
            ip: this.state.ip,
            mask: this.state.mask,
            id_lab: this.state.id_lab
        }
    }

    updateNetwork(network) {
        this.setState({
            name: network.name,
            mask: network.mask.replace('/', ''),
            networkUpdateId: network.id,
        });
    }

    deleteNetwork(idNetwork) {
        let _dataprovider = new Dataprovider()
        _dataprovider.deleteNetworkFromAPI(idNetwork).then(
            (result) => {
                window.location.reload(false)
            },
            (error) => {
                console.log(error)
            },
        )
    }

    render() {
        return (<div hidden={this.state.isHidden}>
            <form onSubmit={this.handleSubmit}>
                <input type="hidden" name="id_lab" value={this.state.id_lab} onChange={this.handleChange}/>
                <div>
                    <label>Name : </label>
                    <input type="text" name="name" value={this.state.name} onChange={this.handleChange}/>
                </div>
                <div>
                    <label>IP : </label>
                    <input type="text" name="ip" value={this.state.ip} onChange={this.handleChange}/>
                </div>
                <div>
                    <label>Mask : </label>
                    / <input type="number" min="1" max="32" name="mask" value={this.state.mask}
                             onChange={this.handleChange}/>
                </div>
                <input type="submit" value="Envoyer"/>
            </form>
            <ul>
                {this.state.networks.map((network, index) => (
                    <li>
                        {network.name} ({network.mask}) -> IP : {network.ip}
                        <button onClick={() => this.updateNetwork(network)}>Update</button>
                        <button onClick={() => this.deleteNetwork(network.id)}>Delete</button>
                    </li>
                ))}
            </ul>
        </div>);
    }
}