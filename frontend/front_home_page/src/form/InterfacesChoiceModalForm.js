import React, {useState, useEffect} from 'react'

export class InterfacesChoiceModalForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isHidden: props.isHidden,
            interfacesList1: [],
            interfacesList2: [],
            connection: {machine1: null, machine2: null}
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClose = this.handleClose.bind(this)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.isHidden !== this.props.isHidden) {
            this.setState({
                isHidden: this.props.isHidden
            });
        }
    }

    handleChange(event) {

    }

    handleClose() {
        this.setState({
            isHidden: true
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let choice1 = event.target.elements.interface1.value
        let choice2 = event.target.elements.interface2.value
        if (choice1 !== "" && choice2 !== "") {
            alert(choice1 + " - " + choice2);
            this.setState({
                isHidden: true,
                connection: {machine1: choice1, machine2: choice2}
            });
        } else {
            alert("Please select interfaces.")
        }
    }

    render() {
        return (
            <div className="modal" tabIndex="-1" role="dialog" hidden={this.state.isHidden}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content" >
                        <form onSubmit={this.handleSubmit}>
                            <div className="modal-header">
                                <span className="close" onClick={this.handleClose}>&times;</span>
                                <h1>Interface Choice</h1>
                                <i>Description</i>
                            </div>
                            <div className="modal-body">
                                <hr/>
                                <select name="interface1" id="interface-select-1" onChange={this.handleChange}>
                                    {this.props.interfacesList1.map((el, i) => {
                                        return (<option value={"machine1_eth"+el.interface}>{"eth"+el.interface}</option>)
                                    })}
                                </select>

                                <select name="interface2" id="interface-select-2" onChange={this.handleChange}>
                                    {this.props.interfacesList2.map((el, i) => {
                                        return (<option value={"machine2_eth"+el.interface}>{"eth"+el.interface}</option>)
                                    })}
                                </select> <br/>
                            </div>
                            <div className="modal-footer">
                                <button type='submit'>Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
