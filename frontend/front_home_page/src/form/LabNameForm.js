import React, {useState, useEffect} from 'react'
import Dataprovider from '../dataprovider/Dataprovider.js'

export class LabNameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameIsEdited: false,
            lab_name: props.lab.name,
            lab_id: props.lab.id,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });
    }

    handleSubmit(event) {
        let dataprovider = new Dataprovider();
        dataprovider.modifyLabToAPI({name: this.state.lab_name}, this.state.lab_id).then(
            (result) => {
                if (result.success === false) {
                    alert(Object.values(result.response)[0]);
                } else {
                    this.setState({nameIsEdited: false})
                }
            },
            (error) => {
                alert(error)
            },
        )
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <h1 id="content" className="header__title" hidden={this.state.nameIsEdited}
                    onDoubleClick={() => (this.setState({nameIsEdited: true}))}>
                    {this.state.lab_name}
                </h1>
                <div hidden={!this.state.nameIsEdited}>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="lab_name" value={this.state.lab_name} onChange={this.handleChange}/>
                        <input type="submit" value="Modifier"/>
                    </form>
                </div>
            </div>
        )
    }
}

