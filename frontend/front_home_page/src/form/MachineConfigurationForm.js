import React, {useState, useEffect} from 'react'
import Dataprovider from '../dataprovider/Dataprovider.js'

export class MachineConfigurationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            bridged: false,
            isHidden: true,
            isLoadedM: false,
            isLoadedI: false,
            Ipv6: false,
            id_lab: this.props.id_lab,
            defaultRoute: 0,
            networks: {},
            machine: {},
            machineName: '',
            interfaces: [],
            listeRoute: [],
            id_network: [],
            errors: {},
            id_type: this.props.id_type ?? 1,
        };

        // Instantiate a dataprovider
        this._dataprovider = new Dataprovider()
        this._dataprovider.fetchInfoFromAPI(this.props.id_lab).then(
            (result) => {
                this.setState({
                    networks: result,
                })
                this.setState({
                    isLoadedI: true,
                })
            },
            (error) => {
                this.setState({
                    isError: true,
                })
                this.setState({
                    isLoadedI: true,
                })
            },
        )

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleReset = this.handleReset.bind(this)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.idMachine !== -1) {
            if (
                prevProps.idMachine !== this.props.idMachine &&
                this.props.idMachine !== null
            ) {
                this._dataprovider.fetchMachineFromAPI(this.props.idMachine).then(
                    (result) => {
                        this.setState({
                            machine: result
                        });
                        this.setState({
                            isLoadedM: true
                        });
                    },
                    (error) => {
                        this.setState({
                            isError: true,
                        })
                        this.setState({
                            isLoadedM: true,
                        })
                    },
                )
            }
            if (prevProps.isHidden !== this.props.isHidden) {
                this.setState({
                    isHidden: this.props.isHidden,
                })
            }

            if (this.state.isLoadedI && this.state.isLoadedM && !this.state.isError) {
                this.state.id_type = this.state.machine.id_type;
                this.state.machineName = this.state.machine.name;
                this.state.bridged = this.state.machine.bridged;
                this.state.ipv6 = this.state.machine.isIPV6;
                this.state.defaultRoute = this.state.machine.default_route;
                this.state.interfaces = [];
                this.state.machine.network_interfaces.forEach((nt) => {
                    if (this.state.interfaces.find(element => element == nt) == undefined) {
                        nt.interface = nt.id_type - 1;
                        this.state.interfaces.push(nt);
                    }
                    if (this.state.id_network.find(element => element == nt.id_network) == undefined) {
                        this.state.id_network.push(nt.id_network);
                    }
                });
                this.state.listeRoute = [];
                this.state.listeRoute.push([0, "nothing"]);
                if (this.state.networks != []) {
                    this.state.networks.forEach((nt) => {
                        if (this.state.id_network.find(element => element === nt.id) !== undefined) {
                            nt.network_interfaces.forEach((network_interface) => {
                                if (network_interface.id_machine != this.props.idMachine) { // don't change for  !==
                                    network_interface.ips.forEach((ip) => {
                                        var eth = network_interface.id_type - 1;
                                        this.state.listeRoute.push([network_interface.id, network_interface.machines.name + " - eth" + eth + " - " + ip.value]);
                                    });
                                }
                            });
                        }
                    });
                }
                this.setState({
                    isLoadedM: false
                });
            }
        } else {
            if (prevProps.idMachine !== this.props.idMachine) {
                this.setState(this.resetValues());
            }
        }
        if (this.props.id_type) {
            this.state.id_type = this.props.id_type;
        }
    }

    resetValues() {
        return {
            bridged: false,
            isLoadedM: false,
            isLoadedI: false,
            Ipv6: false,
            defaultRoute: 0,
            machine: {},
            machineName: '',
            interfaces: [],
            listeRoute: [],
            id_network: [],
            errors: {},
            id_type: null,
        };
    }

    handleChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value,
        })
    }

    handleChangeInterface(i, e) {
        let interfacesValues = this.state.interfacesValues
        interfacesValues[i][e.target.name] = e.target.value
        this.setState({interfacesValues})
    }

    handleChangeIp(e, num) {
        let interfaces = this.state.interfaces
        interfaces.forEach((item, index) => {
            if (item.id == e.target.dataset.id) {
                interfaces[index].ips[num].value = e.target.value;
            }
        })
        this.setState({interfaces})
    }

    handleIsIPV6(e, num) {
        let interfaces = this.state.interfaces
        interfaces.forEach((item, index) => {
            if (item.id == e.target.dataset.id) {
                interfaces[index].ips[num].isIPV6 = e.target.value;
            }
        })
        this.setState({interfaces})
    }

    handleChangeInterfaceCheck(i, e) {
        let interfacesValues = this.state.interfacesValues
        interfacesValues[i][e.target.name] = e.target.checked
        this.setState({interfacesValues})
    }

    handleChangeBridge(e) {
        this.setState({
            bridged: e.target.checked,
        })
    }

    handleChangeIpv6(e) {
        this.setState({
            Ipv6: e.target.checked,
        })
    }

    handleChangeDefaultRoute(e) {
        this.setState({
            defaultRoute: e.target.value,
        })
    }

    deleteIp(e, num) {
        this.state.interfaces.forEach((item, index) => {
            if (item.id == e.target.dataset.id) {
                this.state.interfaces[index].ips.splice(num, 1);
            }
        })
        this.setState({
            isLoadedM: false
        });
    }

    handleChangeNetworkName(event) {
        this.state.interfaces.forEach((item, index) => {
            if (item.id == event.target.dataset.id) {
                let interfaces = [...this.state.interfaces];
                let interfaceElement = {...interfaces[index]};
                interfaceElement.id_network = event.target.value;
                interfaceElement.network = this.state.networks.filter(network => network.id == event.target.value)[0];
                interfaces[index] = interfaceElement;
                this.setState({interfaces});
            }
        })
        this.setState({
            isLoadedM: false
        });
    }

    addInterfaceFields() {
        this.setState({
            interfaces: [
                ...this.state.interfaces,
                {
                    id: this.state.interfaces.length === 0 ? 0 : this.state.interfaces[this.state.interfaces.length - 1].id + 1,
                    interface: this.state.interfaces.length,
                    id_machine: this.props.idMachine,
                    id_network: 0,
                    ips: [],
                },
            ],
        })
    }

    addIp(e) {
        this.state.interfaces.forEach((item, index) => {
            if (item.id == e.target.dataset.id) {
                this.state.interfaces[index].ips.push({
                    id: 0,
                    id_interface: e.target.dataset.id,
                    isIPV6: false,
                    value: "",
                });
            }
        })
        this.setState({
            isLoadedM: false
        });
    }

    removeInterfaceFields(i) {
        let interfaces = this.state.interfaces
        interfaces.splice(i, 1)
        this.setState({interfaces})
    }

    getMachine() {
        this.state.interfaces.forEach((item, index) => {
            this.state.interfaces[index].domain = this.state.interfaces[index].id_network !== 0 ? this.state.networks.filter(network => network.id === parseInt(this.state.interfaces[index].id_network))[0].name : this.state.networks[0].name;
            this.state.interfaces[index].ips.filter(ip => ip.value !== "")
                .forEach((ip, i) => {
                    this.state.interfaces[index].ips[i].ip = this.state.interfaces[index].ips[i].value;
                })
        })
        return {
            name: this.state.machineName,
            id_lab: this.state.id_lab,
            machineId: (this.state.machine.id ?? null),
            id_type: this.state.id_type,
            bridged: this.state.bridged,
            networkInterface: this.state.interfaces,
        }
    }

    handleSubmit(event) {
        event.preventDefault()
        this._dataprovider.postMachineToAPI(this.getMachine()).then(
            (result) => {
                if (result.success === false) {
                    alert(Object.values(result.response)[0]);
                } else {
                    window.location.reload(false)
                }
            },
            (error) => {
                console.log(error);
            },
        )
    }

    handleReset(event) {
        event.preventDefault()
        if (window.confirm('Are you sure to reset the form ?') === true) {
            this.setState({
                machineName: '',
                bridged: 0,
                interfaces: [],
                errors: {},
            })
        }
    }

    render() {
        return (
            <form
                id="config-machine"
                className="config-machine"
                onSubmit={this.handleSubmit}
                hidden={this.state.isHidden}
                onReset={this.handleReset}
            >
                <label>
                    Name
                    <input
                        name="machineName"
                        type="text"
                        value={this.state.machineName}
                        onChange={this.handleChange}
                        placeholder={'pc1, pc2, pc3 ...'}
                    />
                    <span style={{color: 'red'}}>{this.state.errors['name']}</span>
                </label>

                <label>
                    Bridged ?
                    <input
                        name="bridged"
                        type="checkbox"
                        checked={this.state.bridged}
                        onChange={(e) => this.handleChangeBridge(e)}
                    />
                    <span style={{color: 'red'}}>{this.state.errors['name']}</span>
                </label>

                <label>
                    IPV6 ?
                    <input
                        name="ipv6"
                        type="checkbox"
                        checked={this.state.Ipv6}
                        onChange={(e) => this.handleChangeIpv6(e)}
                    />
                    <span style={{color: 'red'}}>{this.state.errors['name']}</span>
                </label>

                <label>
                    Default route ?
                    <select onChange={(e) => this.handleChangeDefaultRoute(e)}>
                        {this.state.listeRoute.map((route, index) => (
                            <option selected={route[0] == this.state.defaultRoute} value={route[0]}>{route[1]}</option>
                        ))}
                    </select>
                    <span style={{color: 'red'}}>{this.state.errors['name']}</span>
                </label>

                <div className={'config-interfaces'} hidden={this.state.networks.length <= 0}>
                    {this.state.interfaces.map((element, index) => (
                        <div className="interface-domain-inline" key={index}>
                            <p>Interface eth{index}</p>

                            <div className="d-flex flex-column">
                                <label hidden={true}>
                                    <input
                                        type="text"
                                        name="interface"
                                        value={index}
                                        hidden={true}
                                    />
                                </label>

                                <label>
                                    Domain
                                    <select data-id={element.id} onChange={(event) => this.handleChangeNetworkName(event)}>
                                        {this.state.networks.map((net, index) => (
                                            <option selected={net.id == element.id_network}
                                                    value={net.id}>{net.name}</option>
                                        ))}
                                    </select>
                                </label>

                                <label>
                                    IP Address
                                    {element.ips.map((ip, index) => (
                                        <div>
                                            <input
                                                type="text"
                                                name="ipAddress"
                                                value={ip.value || ''}
                                                data-id={element.id}
                                                onChange={(e) => this.handleChangeIp(e, index)}
                                                placeholder={element.network === undefined ? this.state.networks[0].ip : element.network.ip}
                                            />
                                            <label>
                                                IPV6 ?
                                                <input
                                                    name="ipv6"
                                                    type="checkbox"
                                                    checked={element.isIPV6}
                                                    onChange={(e) => this.handleIsIPV6(e, index)}
                                                />
                                            </label>
                                            <button type="button" data-id={element.id}
                                                    onClick={(e) => this.deleteIp(e, index)}>Delete
                                            </button>
                                        </div>
                                    ))}
                                    <button
                                        className="button btn add"
                                        type="button"
                                        data-id={element.id}
                                        onClick={(e) => this.addIp(e)}
                                    >
                                        Add ip
                                    </button>
                                </label>
                            </div>
                            <div className="button_container">
                                {index ? (
                                    <button
                                        type="button"
                                        className="button remove btn"
                                        onClick={() => this.removeInterfaceFields(index)}
                                    >
                                        Remove
                                    </button>
                                ) : null}
                            </div>
                        </div>
                    ))}
                    <div className="button-section">
                        <button
                            className="button btn add"
                            type="button"
                            onClick={() => this.addInterfaceFields()}
                        >
                            Add
                        </button>
                    </div>
                </div>

                <div className="config-machine__button">
                    <input className="btn" type="reset" value="Reset"/>
                    <input className="btn" type="submit" value="Save"/>
                </div>
            </form>
        )
    }
}
