import React, { Component } from 'react'

// Import Axios
import axios from 'axios'

class UploadFileSys extends Component {
  state = {
    // Initially, no file is selected
    selectedFile: null,
    isError: false,
    errorOutput: 'The format of your file does not match the required format',
  }

  // On file select (from the pop up)
  onFileChange = (event) => {
    // Update the state
    this.setState({ selectedFile: event.target.files[0] })
  }

  // On file upload (click the upload button)
  onFileUpload = () => {
    // Create an object of formData
    const formData = new FormData()

    // Update the formData object
    formData.append(
      'file',
      this.state.selectedFile,
    )

    // Details of the uploaded file
    console.log(this.state.selectedFile)

    // Request made to the backend api
    // Send formData object

    axios.post("http://localhost:8000/api/lab/new/files/import/zip", formData).then(res => {
      window.location.href = "/labs/" + res.data.response;
    });
  }

  // File content to be displayed after
  // file upload is complete
  fileData = () => {
    if (this.state.selectedFile && !this.state.isError) {
      return (
        <fieldset className="filedetails">
          <legend>Your file</legend>

          <h2>File Details:</h2>
          <p>File Name: {this.state.selectedFile.name}</p>
          <p>File Type: {this.state.selectedFile.type}</p>
          <p>
            Last Modified:{' '}
            {this.state.selectedFile.lastModifiedDate.toDateString()}
          </p>
        </fieldset>
      )
    } else if (this.state.isError) {
      return (
        <div className="alert">
          <b>{this.state.errorOutput}</b>
        </div>
      )
    } else {
      return (
        <div>
          <br />
          <h4>Choose before Pressing the Upload button</h4>
        </div>
      )
    }
  }

  render() {
    return (
      <section className="main_container">
        <h1>Upload your Lab</h1>
        <hr className="separator" />
        <div>
          <label className="submit_button" for="labFile">
            Choose your file
          </label>
          <input
            hidden
            id="labFile"
            type="file"
            onChange={this.onFileChange}
            accept="zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed"
          />
        </div>
        {this.fileData()}
        <button className="submit_button" onClick={this.onFileUpload}>
          Upload!
        </button>
      </section>
    )
  }
}

export default UploadFileSys
