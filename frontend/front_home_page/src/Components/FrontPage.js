// React and State Components for React Life Cycle
import React, { useState, useEffect } from 'react'

// Import Identicon
import Identicon from 'react-identicons'

// Import Link from react-router-dom
// Used to create a link to other pages
import { Link } from 'react-router-dom'

// Import _dataprovider from src\Components\dataprovider\dataprovider.js
// This is used to get the data from the API
import Dataprovider from '../dataprovider/Dataprovider.js'

// FrontPage.js
function FrontPage( ) {
  const [error, setError] = useState(null)
  const [isLoaded, setIsLoaded] = useState(false)
  const [items, setItems] = useState([]);

  let _dataprovider = new Dataprovider()

  // Get the data from the API
  useEffect(() => {
    fetch('http://127.0.0.1:8000/api/labs')
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true)
          setItems(result)
        },
        (error) => {
          setIsLoaded(true)
          setError('Connection is not established : 500')
        },
      )
  }, [])

  // Handle Delete
  let handleDelete = (id) => {
    const requestOptions = {
      method: 'DELETE',
    }
    _dataprovider.deleteLabFromAPI(id).then(() => { window.location.reload(false) })
  }

  // Handle Download
  let handleDownload = (id) => {
    const requestOptions = {
      method: 'GET',
    }
    _dataprovider.downloadLabFromAPI(id)
  }

  // Form Renderer
  // Can be used to create a new lab
  let FormRenderer = () => {
    // Create a new lab
    let CreateLab = (event) => {
      event.preventDefault()
      let newLabName = event.target.name.value

      // Request Options that indificate that the request is a POST
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ name: newLabName }),
      }

      // Send the request to the server
      fetch('http://127.0.0.1:8000/api/labs/', requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((result) => {
          console.log(result)
          if (result.success) {
            window.location.reload(false)
          } else {
            alert(result.response.name)
          }
        })
    }

    return (
      <form onSubmit={CreateLab}>
        <label>
          <strong>Lab's name</strong> :
        </label>
        <br></br>
        <input type="text" name="name" className="input_name"></input>
        <hr></hr>
        <div className="button_container">
          <button type="submit" className="submit_button">
            Create it
          </button>
        </div>
          <center>
              <h4>or</h4>
              <Link to={'/upload'}>
                  <button type="submit" className="submit_button">
                      Upload it
                  </button>
              </Link>
          </center>
      </form>
    )
  }

  // Lab List
  // Can be used to display all the labs
  let LabList = () => {
    // Lab Object
    // Can be used to display a lab
    let LabObject = (labs) => {
      return (
        <li className="recent_projects_items" dataId={labs.id} key={labs.id}>
          <strong
            onClick={() => {
              if (
                window.confirm('Are you sure you want to delete this lab ?')
              ) {
                handleDelete(labs.id)
              } else {
                console.log('canceled')
              }
            }}
            id={labs.id}
            className="delete_button"
          >
            X
          </strong>
          <strong
            onClick={() => {
              if (
                window.confirm('Are you sure you want to download this lab ?')
              ) {
                handleDownload(labs.id)
              } else {
                console.log('canceled')
              }
            }}
            id={labs.id}
            className="download_button"
          >
            ⤓
          </strong>
          {/* <img src="https://via.placeholder.com/140x100" alt="Template"></img> */}
          <Link to={'/labs/' + labs.id}>
            <Identicon
              string={labs.name + labs.id}
              size={140}
              palette={['#737373', '#5271FF', '#737373', '#5271FF']}
            />
          </Link>
          <hr />
          <Link to={'/labs/' + labs.id}>
            <h4 className="recent_projects_items_title">{labs.name}</h4>
          </Link>
        </li>
      )
    }

    return (
      <section className="recent_projects_container">
        <fieldset>
          <legend>Recents Labs</legend>
          <div className="recent_projects_container">
            <ul className="recent_projects_list">
              {items.map((labs) => {
                return LabObject(labs)
              })}
            </ul>
          </div>
        </fieldset>
      </section>
    )
  }

  // New Labs Form
  // Can be used to create a new lab
  let NewLabsForm = () => {
    return (
      <section className="new_project_container">
        <fieldset>
          <legend>Create a new Lab</legend>
          <FormRenderer></FormRenderer>
        </fieldset>
      </section>
    )
  }

  if (error) {
    // If there is an error, display the error
    return <div>Error : {error}</div>
  } else if (!isLoaded) {
    // If the data is not loaded yet, display a loading message
    return <div>Loading...</div>
  } else {
    // If the data is loaded, display the data
    return (
      <article className="main_container">
        <h1 className="title">Open or create a Lab</h1>
        <LabList />
        <NewLabsForm />
      </article>
    )
  }
}

// Export the FrontPage component
export { FrontPage }
