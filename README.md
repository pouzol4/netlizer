# NetLizer (2021-2022)

## Sommaire
1. [Infos Générales](#general-info)
2. [Comment utiliser le projet](#comment-utiliser)
3. [Fonctionnalités](#fonctionnalites)

<a name="general-info"></a>
## Infos Générales
### Description
Netlizer est une application permettant de concevoir une topologie de réseau qui pourrait être traduite en fichier pour l'application Kathara. On pourra aussi charger des fichiers Kathara afin de modifier un labo ou de le stocker sur l'application.  
Pour avoir plus de détails sur ce qui est attendu : veuillez consulter l'énoncé dans docs/enonce.pdf.

### Langages et Frameworks
- **Front-end :** React.JS
- **Back-end :** Laravel
- **Base de Données :** MySQL

### Auteurs
- **Chef de Projet :** [Maxime Constans](https://gitlab.com/maxime-constans)
- **Back-end :** [Maxime Constans](https://gitlab.com/maxime-constans), [Thomas Marty](https://gitlab.com/CotoLab), [Hadrien Pouzol](https://gitlab.com/pouzol4)
- **Front-end :** [Marie Taconet](https://gitlab.com/taconet1), [Alexandre Vedrine](https://gitlab.com/HyroHyKen)

<a name="comment-utiliser"></a>
## Comment utiliser le projet ?
### Installation
⚠ Si vous n'êtes pas sur windows, veuillez ne pas lancer le fichier bat mais allez dedans et réaliser les commandes à l'intérieur dans un terminal.

Installer les outils suivants si vous ne les avez pas : 
- Installer NPM avec le lien [suivant](https://nodejs.org/fr/)
- Installer [WAMP](https://www.wampserver.com/) ou [MAMP](https://www.mamp.info/en/downloads/). Si vous ne voulez pas de pile de logiciels, il faudra donc installer [PHP](https://www.php.net/downloads.php) avec la version 8.0 et [MySQL](https://www.mysql.com/fr/downloads/)
- Installer [Composer](https://getcomposer.org/download/)

Vous avez tous les outils donc vous aurez besoin. Il faut maintenant installer le projet :
- Cloner le projet : `git clone https://gitlab.com/pouzol4/netlizer.git` 
- Créer une base de données  
- Copier le fichier `back_end/.env.example` dans le même dossier sous le nom de .env 
- Modifier les lignes propres à la base de données dans le .env (de `DB_CONNECTION` à `DB_PASSWORD`)
- Revenez dans le dossier racine du projet et lancer le fichier init.bat à l'aide de la commande `start init.bat`

### Démarrage 
Le projet est enfin installé. Il reste à démarrer à chaque utilisation les serveurs avec la commande suivante : `start  start.bat`

<a name="fonctionnalites"></a>
## Fonctionnalités
Les routes déjà réalisées se trouvent dans le fichier docs/routes.pdf

### Laboratoire
- [x] Afficher la liste des labs
- [x] Créer un lab
- [x] Afficher un lab
- [ ] Modifier un lab
- [x] Supprimer un lab
- [ ] Faire une capture d'écran
- [ ] Démarrer Kathara à partir de l'interface

### Domaine
Faute de compréhension, le domaine dans le projet a été nommé "network".
- [x] Ajouter un domaine
- [x] Modifier un domaine
- [x] Supprimer un domaine

### Machine
- [x] Ajouter une machine
- [x] Modifier une machine
- [x] Supprimer une machine

### Génération des dossiers lab
- [x] Générer un dossier
- [x] Générer un fichier de configuration
- [x] Générer un fichier host



