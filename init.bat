cd back_end
call composer install
call php artisan migrate
call php artisan db:seed --class=DatabaseSeeder
call php artisan key:generate

cd ../frontend/front_home_page
call npm install